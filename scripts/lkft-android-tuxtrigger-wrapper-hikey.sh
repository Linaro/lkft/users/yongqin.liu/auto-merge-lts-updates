#!/bin/bash -ex

if [ -z "${TUXTRIGGER_CONFIG}" ]; then
    echo "No tuxtrigger configuration file is specified"
    exit 0
fi

apt-get update && apt-get install -y git virtualenv
WORKSPACE_TUXTRIGGER="workspace-tuxtrigger"
virtualenv --python=python3 "${WORKSPACE_TUXTRIGGER}"
# shellcheck source=/dev/null
source "${WORKSPACE_TUXTRIGGER}"/bin/activate
git clone --depth 1 -b tuxtrigger-rw https://gitlab.com/Linaro/tuxtrigger.git "tuxtrigger"
cd "tuxtrigger" && pip install -r requirements.txt && pip install -r requirements-dev.txt && cd -

dir_out="out"
dir_workspace="${PWD}"
mkdir -p "${dir_out}"
./tuxtrigger/run "${TUXTRIGGER_CONFIG}" \
        --disable-plan \
        --pre-submit "${dir_workspace}/scripts/lkft-android-auto-merge-hikey.sh" \
        --plan "${dir_workspace}" \
        --submit=change \
        --output "${dir_out}/output_file.yaml" \
        --log-level=INFO --log-file "${dir_out}/log.txt"
